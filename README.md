Description :

    Menu principal avec choix du mode (endless ou platformer).
    
    Mode Endless :
    
        Mode infini où le personnage cours en continu avec saut, double saut et saut long, sa vitesse augmente tout les 50pts (hors pièces), son score augmente au fur et à mesure du temps
        ainsi qu'en ramassant des pièces. Il y a également deux power up durant chacun 10 secondes (points doublés avec le diamant vert et pics supprimés avec le diamant bleu) ainsi que 
        deux types de pièges (pics et murs, les murs n'existent que sur la version runner)
        
        Version Platformer : Le niveau est constitué de différentes plateformes discontinues de hauteurs différentes
        Version Runner : Le niveau est constitué d'une seule plateforme infinie
        
        Commandes :
            
            Clic gauche ou barre d'espace : saut
            Double clic gauche ou double clic barre d'espace : double saut
            Clic gauche ou barre d'espace maintenus : saut long
            LeftCtrl : S'accroupir (Platformer)
        
    
    Mode Platformer :
    
    
    
    
    
Différents dossiers (dans /MrRed/Assets) :

    Animations : Contient toutes les animations (Stickman squelette dans le dossier /Player)
    
    Audio : Tout les fichiers audios pour les effets sonores et les musiques
    
    Characters : Contient les prefabs des personnages (joueur et ennemis)
    
    Fonts : Différentes polices utilisées pour les menus
    
    Materials : Contient uniquement le "Slippy" pour que le stickman ne s'accroche pas sur le bord des plateformes (en endless platformer)
    
    Palette : Différents Tiles/Sprites pour la constructiondes niveaux du mode platformer
    
    Prefabs : Contient toutes les différentes plateformes et items du mode endless pour la génération en continu et à l'infini
    
    Scenes : Contient toutes les différentes scènes de jeu (modes et menus, le mode platformer se trouve dans le dossier /Niveaux)
    
    Scripts : Contient tout les scripts nécéssaires au bon fonctionnement du jeu
        PlayerControls : Permet le déplacement du stickman en mode Platformer, et gère les transitions entre les animations

        CameraPlatformer : Court script permettant a la camera de suivre le joueur, avec un offset en option

        DogController : Script qui gère un type d'ennemi, lui donnant un déplacement entre 2 bornes min et max
        
        CameraController : Permet à la caméra de suivre le joueur au cours de la partie
        
        CoinGenerator : Script simple permettant d'afficher les 3 pièces générées lorsqu'elles doivent l'être
        
        DeathMenu : Affiche le petit menu lorsque le personnage perd, possibilité de relancer une partie ou de retourner au menu principal
        
        GameManager : Gère le déroulement de la partie, sert à relancer une partie en remettant tout les paramètres à 0
        
        MainMenu : Menu principal, gère la transition entre chaque scène en les chargeant lorsqu'elles sont appelées
        
        ObjectPooler : Permet de récupérer un certain nombre défini de chaque Prefab lorsqu'on lance une partie et de les intégrer à la scene
        
        Parallax : Gère le défilement du fond en "fausse 3D"
        
        PauseMenu : Menu de pause, permet de reprendre la partie, relancer une nouvelle partie ou retourner au menu principal
        
        PickUpCoins : Détecte la collision entre le joueur et une pièce pour ensuite appeler une fonction du ScoreManager qui assigne les points
        
        PlatformDestroyer : Récupère tout les objets ayant le script attaché et les désactive après une certaine position afin de "recycler" ces même objets en cours de partie plutôt
            qu'en instantier des nouveaux (utilisés pour tout les items en plus des plateformes)
        
        PlatformGenerator : Génère les plateformes en leur donnant une position et une hauteur, ajoute aléatoirement des pièges et/ou items etc selon leur probabilité d'apparition
        
        PlayerController : Gère tout ce qui est rattaché au joueur (déplacements, différents types de sauts, bruitages, animations...)
        
        PowerUpManager : Affiche le power up choisit par le script "PowerUps" et lui assigne une action (doubler les points ou faire disparaître les pics)
        
        PowerUps : Choisit aléatoirement un des deux power up puis appelle la fonction d'activation de celui-ci dans "PowerUpManager"
        
        ScoreManager : Incrémente le score en continu à hauteur de 50pts par seconde et s'occupe d'ajouter les points nécéssaires quand le joueur ramasse une pièce ou quand le power up 
            double de points est actif
        
Dossier /MrRed/Build :

    Fichier executable du jeu : StickmanMiniGames