﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    
    public float distanceBetween;
    public float distanceBetweenMin;
    public float distanceBetweenMax;
    public float maxHeightChange;
    public float randomCoin;
    public float randomSpikes;   
    public float powerUpHeight;   
    public float randomPowerUp;    
    public float randomWall;

    public GameObject thePlatform;

    public Transform generationPoint;
    public Transform maxHeightPoint;

    public ObjectPooler[] theObjectPools;
    public ObjectPooler spikePool;
    public ObjectPooler powerUpPool;
    public ObjectPooler wallPool;


    private int platformSelector;

    private float platformWidth;
    private float[] platformWidths;
    private float minHeight;
    private float maxHeight;
    private float heightChange;

    private CoinGenerator theCoinGenerator;

    // Start is called before the first frame update
    void Start()
    {
        //taille d'une certaine plateforme 
        platformWidths = new float[theObjectPools.Length];

        //initialisation du tableau de taille des plateformes
        for (int i = 0; i < theObjectPools.Length; i++)
        {
            platformWidths[i] = theObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }

        //hauteur min et max d'une plateforme sur l'écran
        minHeight = transform.position.y;
        maxHeight = maxHeightPoint.position.y;

        //générateur de pièces
        theCoinGenerator = FindObjectOfType<CoinGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        //si la position en x est inférieur à celle de generationPoint on crée une plateforme
        if (transform.position.x < generationPoint.position.x)
        {
            //choix de la distance entre les deux plateformes
            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);

            //choix de la plateforme à faire apparaitre
            platformSelector = Random.Range(0, theObjectPools.Length);

            //différence de hauteur entre les deux plateformes
            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);

            //si la différence est supérieur au max on la remplace par la hauteur max
            if (heightChange > maxHeight)
            {
                heightChange = maxHeight;
            }
            //si elle est inférieur au min on la remplace par la hauteur min
            else if (heightChange < minHeight)
            {
                heightChange = minHeight;
            }

            //probabilité d'apparition d'un power up, il faut que le nb tiré aléatoirement soit en dessous de la proba max randomPowerUp
            if (Random.Range(0f, 100f) < randomPowerUp)
            {
                //on récupère le power up choisit, lui donne une position et le fait apparaître
                GameObject newPowerUp = powerUpPool.GetPooledObject();
                newPowerUp.transform.position = transform.position + new Vector3(0f, powerUpHeight, 0f);
                newPowerUp.SetActive(true);
            }

            //on avance la position du générateur de plateforme
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2) + distanceBetween, heightChange, transform.position.z);

            //on récupère la plateforme séléctionné juste avant
            GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();

            //on lui donne sa position et l'affiche
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            //probabilité d'apparition des pièces, si le nombre tiré est bon on affiche les pièces
            if (Random.Range(0f, 100f) < randomCoin)
            {
                theCoinGenerator.SpawnCoins(new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z));
            }

            //probabilté d'apparition des pics, même chose que les pièces et power up
            if (Random.Range(0f, 100f) < randomSpikes)
            {
                //on récupère les pics et choisit leur position sur la plateforme
                GameObject newSpikes = spikePool.GetPooledObject();
                float spikesXPosition = Random.Range(-platformWidths[platformSelector] / 2f + 1f, platformWidths[platformSelector] / 2f - 1f);

                //on leur donne une position
                Vector3 spikesPosition = new Vector3(spikesXPosition, 0.5f, 0f);

                //on affiche les pics
                newSpikes.transform.position = transform.position + spikesPosition;
                newSpikes.transform.rotation = transform.rotation;
                newSpikes.SetActive(true);
            }

            //même chose que pour les pics pour les murs (mode runner uniquement)
            if (Random.Range(0f, 100f) < randomWall)
            {
                GameObject newWall = wallPool.GetPooledObject();
                float wallXPosition = Random.Range(-platformWidths[platformSelector] / 2f + 1f, platformWidths[platformSelector] / 2f - 1f);

                Vector3 wallPosition = new Vector3(wallXPosition, 0.5f, 0f);

                newWall.transform.position = transform.position + wallPosition;
                newWall.transform.rotation = transform.rotation;
                newWall.SetActive(true);
            }

            //on prend la nouvelle position
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2) + distanceBetween, transform.position.y, transform.position.z);

        }
    }
}
