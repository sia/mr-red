﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public PlayerController thePlayer;

    private Vector3 lastPlayerPosition;

    private float distanceToMove;

    // Start is called before the first frame update
    void Start()
    {
        //récupère le joueur et sauvegarde sa position
        thePlayer = FindObjectOfType<PlayerController>();
        lastPlayerPosition = thePlayer.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //distance de déplacement
        distanceToMove = thePlayer.transform.position.x - lastPlayerPosition.x;

        //modifie la distance de déplacement de la caméra et récupère la position du joueur
        transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);
        lastPlayerPosition = thePlayer.transform.position;
    }
}
