﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    private bool doublePoints;
    private bool safeMode;
    private bool powerUpActive;
    private float powerUpTimeCounter;
    private float normalPointsSecond;
    private float spikesRate;
  
    private ScoreManager theScoreManager;
    private PlatformGenerator thePlatformGenerator;
    private PlatformDestroyer[] spikeList;
    private GameManager theGameManager;

    // Start is called before the first frame update
    void Start()
    {
        //récupère le score manager, le generateur de plateformes et le game manager
        theScoreManager = FindObjectOfType<ScoreManager>();
        thePlatformGenerator = FindObjectOfType<PlatformGenerator>();
        theGameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //si un power up est actif
        if (powerUpActive)
        {
            //on lance le compte à rebours du temps du power up
            powerUpTimeCounter -= Time.deltaTime;

            //s'il doit être remis à 0 on met le compteur à 0
            if (theGameManager.powerUpReset)
            {
                powerUpTimeCounter = 0;
                theGameManager.powerUpReset = false;
            }

            //si le power up est le doubleur de points
            if (doublePoints)
            {
                //on double le nombre de points par seconde
                theScoreManager.pointsPerSecond = normalPointsSecond * 2f;
                theScoreManager.shouldDouble = true;
            }


            //si le power up est le "safe mode"
            if (safeMode)
            {
                //on désactive tout les pics
                thePlatformGenerator.randomSpikes = 0f;
            }
            
            //si le compteur passe à 0 on remet tout "normalement"
            if(powerUpTimeCounter <= 0)
            {
                theScoreManager.pointsPerSecond = normalPointsSecond;
                theScoreManager.shouldDouble = false;
                thePlatformGenerator.randomSpikes = spikesRate;
                
                powerUpActive = false;
            }
        }
    }

    public void ActivatePowerUp(bool points, bool safe, float time)
    {
        doublePoints = points;
        safeMode = safe;
        powerUpTimeCounter = time;

        powerUpActive = true;
        normalPointsSecond = theScoreManager.pointsPerSecond;
        spikesRate = thePlatformGenerator.randomSpikes;

        if(safeMode)
        {
            spikeList = FindObjectsOfType<PlatformDestroyer>();
            for (int i = 0; i < spikeList.Length; i++)
            {
                if (spikeList[i].gameObject.name.Contains("Spike"))
                {
                    spikeList[i].gameObject.SetActive(false);
                }
            }
        }
       

    }
}
