﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogController : MonoBehaviour
{

    public float max_X;
    public float min_X;

    private Rigidbody2D rb;

    private bool facingRight;

    void Flip() // Fonction qui retourne le sprite (Marche pas avec les animations)
    {
        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponentInParent<Rigidbody2D>();
        facingRight = true;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            print("ded");
        }     
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x<max_X && facingRight == true)
            rb.velocity = new Vector2(30, rb.velocity.y);
        else if(transform.position.x >= max_X)
        {
            Flip();
            rb.velocity = new Vector2(30 * -1, rb.velocity.y);
            facingRight = false;
        }
        else if (transform.position.x > min_X && facingRight == false)
            rb.velocity = new Vector2(30 * -1, rb.velocity.y);
        else if (transform.position.x <= min_X)
        {
            Flip();
            rb.velocity = new Vector2(30, rb.velocity.y);
            facingRight = true;
        }
    }
}
