﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private Animator anim; // Reference to the animator

    private bool facingRight; // Flag to check what direction the character is facing
    private bool onGround; // Flag to check whether the character is on the ground
    private bool isCrouching; // Flag to check wheter the character is crouching
    private bool isDoubleJumping;
    private bool isRunning;

    private float fallSpeed; // The speed the character falls
    private float verticalMovement; // The amount of vertical movement
    private float crouchMovement; // The amount of crouching movement
    private float baseMovement; // The amount of standing movement

    private Rigidbody2D rb;

    void Flip() // Fonction qui retourne le sprite (Marche pas avec les animations)
    {
        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.parent.localScale;
        theScale.x *= -1;
        transform.parent.localScale = theScale;
    }

    // Start is called before the first frame update
    void Start()
    {
        onGround = true; // Mon equivalent de ta variable stoppedJumping
        isCrouching = false; //Pas utile pour le endless
        facingRight = true; // Pour gerer la direction des deplacements (Pas utile pour le endless)
        isRunning = false;

        fallSpeed = 0.02f;
        baseMovement = verticalMovement = crouchMovement = 0f;

        rb = GetComponentInParent<Rigidbody2D>();

        anim = GetComponentInParent<Animator>();

        //m_Collider = GetComponentInParent<CapsuleCollider2D>();

        //m_x = m_Collider.size.x;
        //m_y = m_Collider.size.y;
    }



    // Update is called once per frame
    void FixedUpdate()
    {

        float h = Input.GetAxis("Horizontal");

        int i = 0;

        // JUMP       (Le DOUBLE JUMP FONCTIONNE MAIS L'ANIMATION NE JOUE PAS TOUT LE TEMPS)

        if (Input.GetKeyDown(KeyCode.Space) == true && onGround == true && isCrouching == false && baseMovement <= 1)
        {
            verticalMovement = 1f;
            onGround = false;
            isDoubleJumping = false;

            i = 0;

            rb.velocity = new Vector2(rb.velocity.x, 0);

            if (facingRight)
                rb.AddForce(new Vector3(rb.velocity.x / 3, 50, 0), ForceMode2D.Impulse);
            else
                rb.AddForce(new Vector3(rb.velocity.x / 3, 50, 0), ForceMode2D.Impulse);
        }
        else if (Input.GetKeyDown(KeyCode.Space) == true && onGround == false && isDoubleJumping == false && i!=0)
        {
            verticalMovement = 3f;
            isDoubleJumping = true;
            print("hello");

            rb.velocity = new Vector2(rb.velocity.x, 0);

            if (facingRight)
                rb.AddForce(new Vector3(rb.velocity.x / 5, 50, 0), ForceMode2D.Impulse);
            else
                rb.AddForce(new Vector3(rb.velocity.x / 5, 50, 0), ForceMode2D.Impulse);
        }
        else if (Input.GetKeyDown(KeyCode.Space) == true && isRunning == true && onGround == true && isDoubleJumping == false)
        {
            verticalMovement = 1.5f;
            onGround = false;

            rb.velocity = new Vector2(rb.velocity.x, 0);

            if (facingRight)
                rb.AddForce(new Vector3(rb.velocity.x / 3, 50, 0), ForceMode2D.Impulse);
            else
                rb.AddForce(new Vector3(rb.velocity.x / 3, 50, 0), ForceMode2D.Impulse);
        }
        else
        {
            // Check if the character is in the air and the vertical movement greater than 0
            if (onGround == false && verticalMovement > 0)
            {

                i = 1;

                // Reduce vertical movement
                verticalMovement -= fallSpeed;
                if (verticalMovement < 0)
                {
                    verticalMovement = 0;
                    onGround = true;
                    isDoubleJumping = false;
                    i = 0;
                }
            }
        }

        //FIN JUMP

        // WALK 

        if (Input.GetKey(KeyCode.D) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.Q) == false)
        {
            if (facingRight == false)
            {
                Flip();
                facingRight = true;
            }

            baseMovement = 1f;

            rb.velocity = new Vector2(h*10, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D) == true && Input.GetKey(KeyCode.LeftControl) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.Q) == false)
        {
            if (facingRight == false)
            {
                Flip();
                facingRight = true;
            }

            baseMovement = 0f;
            isCrouching = true;
        }
        else if (Input.GetKey(KeyCode.Q) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.D) == false)
        {
            if (facingRight == true)
            {
                Flip();
                facingRight = false;
            }

            baseMovement = 1f;

            rb.velocity = new Vector2(h*10, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.Q) == true && Input.GetKey(KeyCode.LeftControl) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.D) == false)
        {
            if (facingRight == true)
            {
                Flip();
                facingRight = false;
            }

            baseMovement = 0f;
            isCrouching = true;
        }
        else if (Input.GetKey(KeyCode.D) == false && isCrouching == false && onGround == true && baseMovement > 0 && Input.GetKey(KeyCode.Q) == false)
        {
            baseMovement = 0f;
        }


        // RUN

        if (Input.GetKey(KeyCode.D) == true && Input.GetKey(KeyCode.LeftShift) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.Q) == false)
        {
            if (facingRight == false)
            {
                Flip();
                facingRight = true;
            }

            baseMovement = 2f;
            isRunning = true;

            rb.velocity = new Vector2(h * 40, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D) == true && Input.GetKey(KeyCode.LeftShift) == false && isCrouching == false && onGround == true && Input.GetKey(KeyCode.Q) == false)
        {
            if (facingRight == false)
            {
                Flip();
                facingRight = true;
            }

            baseMovement = 1f;
            isRunning = false;

        }
        else if (Input.GetKey(KeyCode.Q) == true && Input.GetKey(KeyCode.LeftShift) == true && isCrouching == false && onGround == true && Input.GetKey(KeyCode.D) == false)
        {
            if (facingRight == true)
            {
                Flip();
                facingRight = false;
            }

            baseMovement = 2f;
            isRunning = true;

            rb.velocity = new Vector2(h * 40, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.Q) == true && Input.GetKey(KeyCode.LeftShift) == false && isCrouching == false && onGround == true && Input.GetKey(KeyCode.D) == false)
        {
            if (facingRight == true)
            {
                Flip();
                facingRight = false;
            }

            baseMovement = 1f;
            isRunning = false;
        }
        else if (Input.GetKey(KeyCode.D) == false && Input.GetKey(KeyCode.Q) == false && Input.GetKey(KeyCode.LeftShift) == false && isCrouching == false && onGround == true)
        {
            baseMovement = 0;
            isRunning = false;
        }



        // CROUCHING    (L'ANIMATION NE FLIP PAS COMPLETEMENT ET STAND EST BUGGUEE)

        if (Input.GetKey(KeyCode.LeftControl) == true && onGround == true && baseMovement <= 1 && isCrouching == false)
        {
            isCrouching = true;
            baseMovement = 0f;
        }

        if (Input.GetKey(KeyCode.LeftControl) == true && Input.GetKey(KeyCode.D) == true && onGround == true && isCrouching == true && Input.GetKey(KeyCode.Q) == false)
        {
            if (facingRight == false)
            {
                Flip();
                facingRight = true;
            }

            crouchMovement = 1f;

            rb.velocity = new Vector2(h*10, rb.velocity.y);

        }
        else if (Input.GetKey(KeyCode.LeftControl) == true && Input.GetKey(KeyCode.Q) == true && onGround == true && isCrouching == true && Input.GetKey(KeyCode.D) == false)
        {
            if (facingRight == true)
            {
                Flip();
                facingRight = false;
            }

            crouchMovement = 1f;

            rb.velocity = new Vector2(h * 10, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.LeftControl) == true && Input.GetKey(KeyCode.D) == false && Input.GetKey(KeyCode.Q) == false && onGround == true && isCrouching == true)
        {
            crouchMovement = 0f;
        }
        else if (Input.GetKey(KeyCode.LeftControl) == false && onGround == true && isCrouching == true)
        {
            crouchMovement = 0f;
            isCrouching = false;

            //m_Collider.size = new Vector2(m_x, m_y);
        }

        // FIN CROUCHING


        // GESTION DEATH AVEC COLLISIONS



        // FIN GESTION DEATH

        anim.SetBool("onGround", onGround);
        anim.SetBool("isCrouching", isCrouching);
        anim.SetBool("isRunning", isRunning);
        anim.SetBool("isDoubleJumping", isDoubleJumping);
        anim.SetFloat("verticalMovement", verticalMovement);
        anim.SetFloat("crouchMovement", crouchMovement);
        anim.SetFloat("baseMovement", baseMovement);


    }

}
