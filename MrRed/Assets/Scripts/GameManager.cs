﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public Transform platformGenerator;
    public PlayerController thePlayer;
    public DeathMenu deathScreen;

    public bool powerUpReset;


    private Vector3 platformStartPoint;
    private Vector3 playerStartPoint;
 

    private PlatformDestroyer[] platformList;
    private ScoreManager theScoreManager;
    private Parallax[] parallaxList;

    private Vector3[] parallaxStartPoint;


    // Start is called before the first frame update
    void Start()
    {
        //récupère la position initiale du générateur de plateforme, du joueur et initialise le score manager
        platformStartPoint = platformGenerator.position;
        playerStartPoint = thePlayer.transform.position;
        theScoreManager = FindObjectOfType<ScoreManager>();
        parallaxList = FindObjectsOfType<Parallax>();
        parallaxStartPoint = new Vector3[parallaxList.Length];

        for (int i = 0; i < parallaxList.Length; i++)
        {
            parallaxStartPoint[i] = parallaxList[i].transform.position;
        }
    }

    //stoppe l'incrémentation du score, masque le joueur et affiche l'écran de mort
    public void RestartGame()
    {
        theScoreManager.scoreIncreasing = false;
        thePlayer.gameObject.SetActive(false);

        deathScreen.gameObject.SetActive(true);
    }

    //réinitialise les données du jeu
    public void Reset()
    {
        //désactive l'écran de mort
        deathScreen.gameObject.SetActive(false);

        //récupère le destructeur de plateformes
        platformList = FindObjectsOfType<PlatformDestroyer>();

        //efface toutes les plateformes
        for (int i = 0; i < platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive(false);
        }


        for (int i = 0; i < parallaxList.Length; i++)
        {
            parallaxList[i].transform.position = parallaxStartPoint[i];
        }

        //repositionne le joueur à la position de départ
        thePlayer.transform.position = playerStartPoint;
        //repositionne le générateur de plateforme
        platformGenerator.position = platformStartPoint;
        //réaffiche le joueur
        thePlayer.gameObject.SetActive(true);

        //remet le score à 0 et réactive son incrémentation
        theScoreManager.scoreCount = 0;
        theScoreManager.scoreIncreasing = true;

        //réinitialise les power up (désactivation si un power up est possédé)
        powerUpReset = true;
    }
}
