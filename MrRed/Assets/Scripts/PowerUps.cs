﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public bool doublePoints;
    public bool safeMode;

    public float powerUpTime;

    public Sprite[] powerUpTab;

    private AudioSource powerUpSound;
    private PowerUpManager thePowerUpManager;

    // Start is called before the first frame update
    void Start()
    {
        //récupère le manager de power up et le son du power up
        thePowerUpManager = FindObjectOfType<PowerUpManager>();
        powerUpSound = GameObject.Find("PowerUpSound").GetComponent<AudioSource>();
    }

    private void Awake()
    {
        //on choisie le power up
        int powerUpSelector = Random.Range(0, 2);

        switch (powerUpSelector)
        {
            case 0: doublePoints = true;
                break;

            case 1: safeMode = true;
                break;
        }
        
        //on récupère l'image du power up choisi 
        GetComponent<SpriteRenderer>().sprite = powerUpTab[powerUpSelector];
    }

    //si l'objet entré en contact avec le power up est le joueur alors on active
    //le power up correspondant et on le masque
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            powerUpSound.Play();
            thePowerUpManager.ActivatePowerUp(doublePoints, safeMode, powerUpTime);
        }

        gameObject.SetActive(false);
    }
}
