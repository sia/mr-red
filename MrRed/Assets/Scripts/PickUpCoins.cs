﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpCoins : MonoBehaviour
{
    public int scoreToGive;

    private ScoreManager theScoreManager;
    private AudioSource coinSound;

    // Start is called before the first frame update
    void Start()
    {
        //récupère le score manager et le son des pièces
        theScoreManager = FindObjectOfType<ScoreManager>();
        coinSound = GameObject.Find("CoinSound").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //si l'objet qui entre en contact avec la pièce est le joueur alors on donne le score correspondant, masque la pièce récupérée
    //et joue le son de la pièce
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name == "Player")
        {
            theScoreManager.AddScore(scoreToGive);
            gameObject.SetActive(false);

            if (coinSound.isPlaying)
            {
                coinSound.Stop();
                coinSound.Play();
            }
            else
            {
                coinSound.Play();
            }  
        }
    }
}
