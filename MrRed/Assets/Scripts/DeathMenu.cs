﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    public string mainMenuLevel;

    //réinitialise les données de la partie pour recommencer
    public void RestartGame()
    {
        FindObjectOfType<GameManager>().Reset();
    }

    //retourne au menu principal
    public void QuitToMain()
    {
        SceneManager.LoadScene(mainMenuLevel);
    }
}
