﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//supposé faire défiler l'image de fond avec un effet de fausse 3D mais n'a finalement pas été utilisé
public class Parallax : MonoBehaviour
{
    new public GameObject camera;
    public float parallaxEffect;

    private float length, startPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float movedDistance = (camera.transform.position.x * (1 - parallaxEffect));
        float distance = (camera.transform.position.x * parallaxEffect);

        transform.position = new Vector3(startPos + distance, transform.position.y, transform.position.z);
    
        if(movedDistance > startPos + length)
        {
            startPos += length;
        }
    }
}
