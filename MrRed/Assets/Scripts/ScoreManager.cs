﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    public Text scoreText;
    public Text highScoreText;

    public float scoreCount;
    public float highScoreCount;
    public float pointsPerSecond;

    public bool scoreIncreasing;
    public bool shouldDouble;

    // Start is called before the first frame update
    void Start()
    {
        //si le high score existe déjà on récupère sa valeur
        if(PlayerPrefs.HasKey("HighScore"))
        {
            highScoreCount = PlayerPrefs.GetFloat("HighScore");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //si le score est en train d'augmenter
        if (scoreIncreasing)
        {
            //on donne le nombre de points par seconde (multiplié par le temps pour voir le score augmenter en temps réel)
            scoreCount += pointsPerSecond * Time.deltaTime;
        }

        //si le score actuel est supérieur au high score on remplace ce dernier par le score actuel
        if(scoreCount > highScoreCount)
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", highScoreCount);
        }

        //affiche le score actuel et le high score
        scoreText.text = "Score: " + Mathf.Round(scoreCount);
        highScoreText.text = "High Score: " + Mathf.Round(highScoreCount);
    }

    public void AddScore(int pointsToAdd)
    {
        //si le power up doubleur de points est actif on double le nombre de points à gagner
        if (shouldDouble)
        {
            pointsToAdd = pointsToAdd * 2;
            scoreCount += pointsToAdd;
        }
        else
        {
            scoreCount += pointsToAdd;
        }
    }
}
