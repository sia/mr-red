﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public GameObject pooledObject;
    public int pooledAmount;

    List<GameObject> pooledObjects;

    // Start is called before the first frame update
    void Start()
    {
        //crée une liste avec la bibliothèque d'objets
        pooledObjects = new List<GameObject>();

        //on crée un nombre pooledAmount d'objet
        for(int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject);

            //on les masques
            obj.SetActive(false);
            //on les ajoute à la liste d'objets récupérés
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        //renvoie un objet inactif (est appelé pour "générer" les objets, mais réutilise en fait les mêmes objets à l'infini) 
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        //récupère l'objet puis le désactive le met dans la liste et le renvoie
        GameObject obj = (GameObject)Instantiate(pooledObject);

        obj.SetActive(false);
        pooledObjects.Add(obj);

        return obj;
    }
    
}
