﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public string mainMenuLevel;
    public GameObject pauseMenu;
    
    //stoppe le temps et active le menu de pause
    public void PauseGame()
    {
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
    } 

    //réactive le temps et désactive l'écran de pause
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(true);
    }

    //réactive le temps et lance une nouvelle partie
    public void RestartGame()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        FindObjectOfType<GameManager>().Reset();
    }

    //retourne au menu princicpal
    public void QuitToMain()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenuLevel);
    }
}
