﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string platformerMode;
    public string endlessMode;
    public string platformer;
    public string runner;
    public string tutorial;
    public string firstLevel;
    public string sndLevel;
    public string mainMenu;

    //charge la scène choisie ou quitte le jeu
    public void EndlessMode()
    {
        SceneManager.LoadScene(endlessMode);
    }

    public void PlatformerMode()
    {
        SceneManager.LoadScene(platformerMode);
    }

    public void PlayPlatformer()
    {
        SceneManager.LoadScene(platformer);
    }

    public void PlayRunner()
    {
        SceneManager.LoadScene(runner);
    }

    public void Tutorial()
    {
        SceneManager.LoadScene(tutorial);
    }

    public void FirstLevel()
    {
        SceneManager.LoadScene(firstLevel);
    }

    public void SndLevel()
    {
        SceneManager.LoadScene(sndLevel);
    }

    public void QuitToMain()
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
