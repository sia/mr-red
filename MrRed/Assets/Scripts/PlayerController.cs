﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed ;
    public float jumpForce;
    public float groundCheckRadius;
    public float jumpTime;
    public float speedMultiplier;
    public float speedIncreasePoint;

    public bool grounded;

    public LayerMask whatIsGround;
    public Transform groundCheck;
    public GameManager theGameMananager;
    public AudioSource jumpSound;
    public AudioSource deathSound;
    

    private Rigidbody2D myRigidbody ;
    private Animator myAnimator;

    private float jumpTimeCounter;
    private float speedPointCount;
    private float moveSpeedStore;
    private float speedPointCountStore;
    private float speedIncreasePointStore;

    private bool stoppedJumping;
    private bool canDoubleJump;


    // Start is called before the first frame update
    void Start()
    {
        //récupère le rigidbody du joeur
        myRigidbody = GetComponent<Rigidbody2D>();

        //récupère les animations
        myAnimator = GetComponent<Animator>();

        //temps de saut
        jumpTimeCounter = jumpTime;

        //point d'accélération du joueur
        speedPointCount = speedIncreasePoint;

        //sauvegarde la vitesse du joueur
        moveSpeedStore = movementSpeed;
        speedPointCountStore = speedPointCount;
        speedIncreasePointStore = speedIncreasePoint;

        stoppedJumping = true;
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        //si on a passé le point d'accélération on modifie la vitesse
        if(transform.position.x > speedPointCount)
        {
            speedPointCount += speedIncreasePoint;
            speedIncreasePoint = speedIncreasePoint * speedMultiplier;
            movementSpeed = movementSpeed * speedMultiplier;
        }

        //associe la vitess au joueur
        myRigidbody.velocity = new Vector2(movementSpeed, myRigidbody.velocity.y);

        //si on appuie sur espace ou clic gauche
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) )
        {
            //si le joueur est au sol on lance le son de saut et lui donne la puissance de saut au joueur
            if(grounded)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                stoppedJumping = false;
                jumpSound.Play();
            }  

            //si le joueur est déjà en train de sauter et qu'il peut faire un double saut il re-saute
            if(!grounded && canDoubleJump)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                jumpTimeCounter = jumpTime;
                stoppedJumping = false;
                canDoubleJump = false;
                jumpSound.Play();
            }
        }

        //les deux if suivants gèrent le temps de saut : plus la touche reste appuyé plus le joueur saute haut et longtemps
        if((Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0)) && !stoppedJumping)
        {
            if(jumpTimeCounter > 0)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
        }

        if(Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0))
        {
            jumpTimeCounter = 0;
            stoppedJumping = true;
        }

        //quand le joueur est au sol au réinitialise son temps de saut et la possibilité de faire un double saut
        if(grounded)
        {
            jumpTimeCounter = jumpTime;
            canDoubleJump = true;
        }

        //sert aux transitions entre les animations, si la vitesse dépasse un certain point le joueur cours et s'il n'est pas au sol il saute
        myAnimator.SetFloat("Speed", myRigidbody.velocity.x);
        myAnimator.SetBool("Grounded", grounded);
    }

    
    void OnCollisionEnter2D(Collision2D other)
    {
        //si le joueur entre en contact avec des pics (ou un mur en mode runner) ou s'il tombe 
        if (other.gameObject.tag == "killbox")
        {
            //on joue le son de mort
            deathSound.Play();
            //on relance une partie
            theGameMananager.RestartGame();
            //et on réinitialise la vitesse
            movementSpeed = moveSpeedStore;
            speedPointCount = speedPointCountStore;
            speedIncreasePoint = speedIncreasePointStore;
        }
    }
}
